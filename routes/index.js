var express = require('express');
var router = express.Router();
var _ = require('lodash');
var FeedParser = require('feedparser');
var request = require('request');
var moment = require('moment');
moment.locale('RU_ru');

var blocks = [
  {
    id: 0,
    title: 'Time',
    content: moment().format("DD MMMM, HH:mm")
  },
  {
    id: 1,
    title: 'Habrahabr',
    url: 'http://habrahabr.ru/rss/interesting/'
  },
  {
    id: 2,
    title: 'TSN',
    url: 'http://tsn.ua/rss'
  },
  {
    id: 3,
    title: 'Node.js',
    url: 'http://blog.nodejs.org/feed/'
  },
  {
    id: 4,
    title: 'DOU Events',
    url: 'http://dou.ua/calendar/feed/'
  },
  {
    id: 5,
    title: 'Stratoplan',
    url: 'http://blog.stratoplan.ru/feed/'
  },
  {
    id: 6,
    title: "DOU Forum",
    url: 'http://dou.ua/forums/feed/'
  },
  {
    id: 7,
    title: "Intuit News",
    url: "www.intuit.ru/news/rss"
  }
];

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: 'RSSReader',
    blocks: blocks
  });
});

router.get('/block/:id', function(req, res, next) {
  var feed = _.findWhere(blocks, {id: Number.parseInt(req.params.id)});
  readFeed(feed.url, function(err, items){
    res.json(items.slice(0,10));
  });
});

function readFeed(url, callback) {

  var feedparser = new FeedParser();
  var req = request.get(url);
  var items = [];

  req.on('error', callback);
  req.on('response', function processResponse(res) {
    if (res.statusCode != 200) {
      return this.emit('error', new Error('Bad status code'));
    } else {
      this.pipe(feedparser);
    }
  });


  feedparser.on('error', callback);
  feedparser.on('readable', function processData() {
    // This is where the action is!
    var item;
    while (item = this.read()) {
      console.log(item.title);
      item.pubdate = moment(item.pubdate).fromNow();
      item.title = item.title.replace(/\[.*?\]/, '');
      items.push(item);
    }
  });
  feedparser.on('end', function processData() {
    console.log(items.length);
    callback(null, items);
  });
}

module.exports = router;
