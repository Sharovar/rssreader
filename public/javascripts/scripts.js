var TEMPLATE = '{{#each items}}' +
    '<p>&bullet; <a href="{{link}}" target="_blank">{{title}}</a> ' +
    '<small>&bullet; {{pubdate}}</small></p>' +
    '{{/each}}';

function loadBlock(id) {
    $.get( "/block/" + id, function(data) {
        var template = Handlebars.compile(TEMPLATE);
        var html = template({items: data});
        $('#feed_' + id).find('.body').html(html);
    });
}
